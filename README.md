# README #

EXCHANGE

### How do I get set up? ###

Database Schema creation and data dump script is attached: schemaAndData.sql

* Build project:
```gradlew clean build```

* Run project:
1. build project

2.```java -jar build/libs/exchange-0.0.1-SNAPSHOT.jar --spring.datasource.url={ your datasource url } --spring.datasource.username={ your db username } --spring.datasource.password={ your db password }}```

ex: ```java -jar build/libs/exchange-0.0.1-SNAPSHOT.jar --spring.datasource.url=jdbc:mysql://localhost:3306/exchange2 --spring.datasource.username=test --spring.datasource.password=test```

App wil start on 8080 port, you can change it specifying ```--server.port={the port you want}``` wen run the app.
### Request Examples: ###
* Update balance:
```
PUT http://localhost:8080/balance
{
	 "employeeId":6,
	 "currencyCode":"MDL",
     "amount":1000
}
```
* Get balance:
```
GET http://localhost:8080/balance/MDL
```

* Add Employee:
```
POST  http://localhost:8080/employee
{
    "firstName": "Frirstname",
    "lastName": "Lastname"
}
```
* Get employee:
```
GET http://localhost:8080/employee/6
```
* Exchange currency:
```
POST http://localhost:8080/exchange 
{
     "employeeId" : 6,
     "sellCurrency":"MDL",
     "buyCurrency":"USD",
     "sellCurrencyAmount":100.5
}
```
* Get currency transaction:
```
GET http://localhost:8080/exchange/transaction/9
```
* Get rate for currency pair:
```
GET http://localhost:8080/rate?sellCurrency=MDL&buyCurrency=USD
```
* Get all rates:
```
GET http://localhost:8080/rate/all
```
* Create rate:
```POST http://localhost:8080/rate/
   {
       "rate": 1,
       "sellCurrency":"EUR",
       "buyCurrency":"USD"
   }
```


