CREATE DATABASE  IF NOT EXISTS `exchange` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `exchange`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: exchange
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `balance`
--

DROP TABLE IF EXISTS `balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balance` (
  `currencyid` varchar(45) NOT NULL,
  `balance` decimal(30,5) NOT NULL,
  PRIMARY KEY (`currencyid`),
  CONSTRAINT `ballancecurrency` FOREIGN KEY (`currencyid`) REFERENCES `currency` (`code`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balance`
--

LOCK TABLES `balance` WRITE;
/*!40000 ALTER TABLE `balance` DISABLE KEYS */;
INSERT INTO `balance` VALUES ('MDL',10372.50000),('USD',486.47000);
/*!40000 ALTER TABLE `balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balancetransaction`
--

DROP TABLE IF EXISTS `balancetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balancetransaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operatorid` int(11) NOT NULL,
  `transaction` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `operators_idx` (`operatorid`),
  KEY `transaction_idx` (`transaction`),
  CONSTRAINT `operator` FOREIGN KEY (`operatorid`) REFERENCES `employee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction` FOREIGN KEY (`transaction`) REFERENCES `transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancetransaction`
--

LOCK TABLES `balancetransaction` WRITE;
/*!40000 ALTER TABLE `balancetransaction` DISABLE KEYS */;
INSERT INTO `balancetransaction` VALUES (1,6,17),(2,6,18),(3,6,19),(4,6,20),(5,6,21),(6,6,22),(7,6,23),(8,6,24),(9,6,25),(10,6,26),(11,6,27),(12,6,28),(13,6,29);
/*!40000 ALTER TABLE `balancetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `code` varchar(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES ('EUR','Euro'),('MDL','Moldovan LEU'),('UAH','Ukrainian hryvnia'),('USD','United States Dollar');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (6,'Frirstname','Lastname');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exchangetransaction`
--

DROP TABLE IF EXISTS `exchangetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exchangetransaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operatorid` int(11) NOT NULL,
  `debittransactionid` int(11) NOT NULL,
  `credittransactionid` int(11) NOT NULL,
  `rate` decimal(30,5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `transations_idx` (`operatorid`),
  KEY `debittransaction_idx` (`debittransactionid`),
  KEY `credittransaction_idx` (`credittransactionid`),
  CONSTRAINT `credittransaction` FOREIGN KEY (`credittransactionid`) REFERENCES `transaction` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `debittransaction` FOREIGN KEY (`debittransactionid`) REFERENCES `transaction` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `transations` FOREIGN KEY (`operatorid`) REFERENCES `employee` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exchangetransaction`
--

LOCK TABLES `exchangetransaction` WRITE;
/*!40000 ALTER TABLE `exchangetransaction` DISABLE KEYS */;
INSERT INTO `exchangetransaction` VALUES (1,6,2,1,0.60000),(2,6,4,3,0.06000),(3,6,6,5,16.00000),(4,6,8,7,16.00000),(5,6,10,9,0.06000),(6,6,12,11,0.06000),(7,6,13,14,0.06000),(8,6,15,16,0.06000),(9,6,30,31,0.06000);
/*!40000 ALTER TABLE `exchangetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rate`
--

DROP TABLE IF EXISTS `rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate` (
  `sellcurrency` varchar(10) NOT NULL,
  `buycurrency` varchar(10) NOT NULL,
  `rate` decimal(30,5) NOT NULL,
  PRIMARY KEY (`sellcurrency`,`buycurrency`),
  KEY `sellcurency_idx` (`sellcurrency`),
  KEY `buycurrency_idx` (`buycurrency`),
  CONSTRAINT `buycurrency` FOREIGN KEY (`buycurrency`) REFERENCES `currency` (`code`) ON UPDATE NO ACTION,
  CONSTRAINT `sellcurency` FOREIGN KEY (`sellcurrency`) REFERENCES `currency` (`code`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate`
--

LOCK TABLES `rate` WRITE;
/*!40000 ALTER TABLE `rate` DISABLE KEYS */;
INSERT INTO `rate` VALUES ('EUR','USD',1.00000),('MDL','USD',0.06000),('USD','EUR',1.00000),('USD','MDL',16.00000);
/*!40000 ALTER TABLE `rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(30,5) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `currency_idx` (`currency`),
  CONSTRAINT `currency` FOREIGN KEY (`currency`) REFERENCES `currency` (`code`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (1,-480.00000,'USD','2020-08-11 01:22:19'),(2,800.00000,'MDL','2020-08-11 01:22:19'),(3,-48.00000,'USD','2020-08-11 01:27:41'),(4,800.00000,'MDL','2020-08-11 01:27:41'),(5,-160.00000,'MDL','2020-08-11 01:28:18'),(6,10.00000,'USD','2020-08-11 01:28:18'),(7,-168.00000,'MDL','2020-08-11 01:28:30'),(8,10.50000,'USD','2020-08-11 01:28:30'),(9,-6.03000,'USD','2020-08-11 01:29:02'),(10,100.50000,'MDL','2020-08-11 01:29:02'),(11,0.00000,'USD','2020-08-11 01:34:48'),(12,0.00000,'MDL','2020-08-11 01:34:48'),(13,0.00000,'MDL','2020-08-11 17:53:29'),(14,0.00000,'USD','2020-08-11 17:53:29'),(15,0.00000,'MDL','2020-08-11 17:54:57'),(16,0.00000,'USD','2020-08-11 17:54:57'),(17,1000.00000,'MDL','2020-08-11 19:11:24'),(18,1000.00000,'MDL','2020-08-11 19:11:32'),(19,1000.00000,'MDL','2020-08-11 19:11:37'),(20,1000.00000,'MDL','2020-08-11 19:11:40'),(21,-1000.00000,'MDL','2020-08-11 22:52:14'),(22,-1000.00000,'MDL','2020-08-11 22:52:18'),(23,-1000.00000,'MDL','2020-08-11 22:52:20'),(24,-1000.00000,'MDL','2020-08-11 22:52:32'),(25,-1000.00000,'MDL','2020-08-11 22:52:33'),(26,-1000.00000,'MDL','2020-08-11 22:52:36'),(27,-1000.00000,'MDL','2020-08-12 02:13:27'),(28,1000.00000,'MDL','2020-08-12 02:13:45'),(29,1000.00000,'MDL','2020-08-12 02:13:47'),(30,0.00000,'MDL','2020-08-12 02:15:02'),(31,0.00000,'USD','2020-08-12 02:15:02');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-12 10:28:32
