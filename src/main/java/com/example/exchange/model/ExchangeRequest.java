package com.example.exchange.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ExchangeRequest {

    @NotNull
    long employeeId;

    @NotNull
    String sellCurrency;

    @NotNull
    String buyCurrency;

    @NotNull
    double amount;
}
