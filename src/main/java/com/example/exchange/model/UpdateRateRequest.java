package com.example.exchange.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode
public class UpdateRateRequest {
    String sellCurrency;
    String buyCurrency;
    double rate;
}
