package com.example.exchange.service;

import com.example.exchange.entity.Rate;
import com.example.exchange.model.UpdateRateRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RateService {
    Rate saveRate(UpdateRateRequest updateRateRequest);

    Rate getRate(String sellCurrencyCode, String buyCurrencyCode);

    List<Rate> getAllRates();
}
