package com.example.exchange.service;

import com.example.exchange.entity.Currency;
import com.example.exchange.entity.CurrencyPair;
import com.example.exchange.entity.Rate;
import com.example.exchange.exception.ExchangeException;
import com.example.exchange.model.UpdateRateRequest;
import com.example.exchange.repository.RateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class RateServiceImpl implements RateService {

    @Autowired
    private RateRepository rateRepository;

    @Autowired
    private CurrencyServiceImpl currencyService;

    @Transactional
    public Rate saveRate(UpdateRateRequest updateRateRequest) {
        Currency sellCurrency = currencyService.getCurrency(updateRateRequest.getSellCurrency());
        Currency buyCurrency = currencyService.getCurrency(updateRateRequest.getBuyCurrency());
        CurrencyPair currencyPair = new CurrencyPair(sellCurrency, buyCurrency);
        Rate rate = new Rate(updateRateRequest.getRate(), currencyPair);
        return rateRepository.save(rate);
    }

    public Rate getRate(String sellCurrencyCode, String buyCurrencyCode) {
        Currency sellCurrency = currencyService.getCurrency(sellCurrencyCode);
        Currency buyCurrency = currencyService.getCurrency(buyCurrencyCode);
        CurrencyPair currencyPair = new CurrencyPair(sellCurrency, buyCurrency);
        return rateRepository.findById(currencyPair).
                orElseThrow(() -> new ExchangeException(String.format("No rate for currency pair: %s - %s ", sellCurrency, buyCurrency)));
    }

    public List<Rate> getAllRates() {
        return rateRepository.findAll();
    }
}
