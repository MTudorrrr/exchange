package com.example.exchange.service;

import com.example.exchange.entity.*;
import com.example.exchange.exception.ExchangeException;
import com.example.exchange.model.UpdateBalanceRequest;
import com.example.exchange.repository.BalanceReRepository;
import com.example.exchange.repository.BalanceTransactionRepository;
import com.example.exchange.repository.TransactionRepository;
import com.example.exchange.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;

@Service
public class BalanceServiceImpl implements BalanceService {

    @Autowired
    private BalanceReRepository balanceReRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private BalanceTransactionRepository balanceTransactionRepository;

    @Autowired
    private CurrencyServiceImpl currencyService;

    @Autowired
    private EmployeeServiceImpl employeeService;

    public Balance getBalance(String currencyCode) {
        return balanceReRepository.findById(currencyCode)
                .orElseThrow(() -> new ExchangeException(String.format("No balance for currency code: %s", currencyCode)));
    }

    public Balance saveBalance(Balance balance) {
        return balanceReRepository.save(balance);
    }

    @Transactional
    public Balance updateBalance(UpdateBalanceRequest updateBalanceRequest) {
        Currency currency = new Currency();
        currency.setCode(updateBalanceRequest.getCurrencyCode());

        Employee employee = new Employee();
        employee.setId(updateBalanceRequest.getEmployeeId());

        Transaction transaction = Utils.createTransaction(currency, updateBalanceRequest.getAmount(), new Timestamp(new Date().getTime()));

        Balance currentBalance = getBalance(updateBalanceRequest.getCurrencyCode());
        currentBalance.setBalance(currentBalance.getBalance() + updateBalanceRequest.getAmount());

        BalanceTransaction balanceTransaction = new BalanceTransaction();
        balanceTransaction.setEmployee(employee);
        balanceTransaction.setTransaction(transaction);

        transactionRepository.save(transaction);
        balanceTransactionRepository.save(balanceTransaction);
        return saveBalance(currentBalance);
    }
}
