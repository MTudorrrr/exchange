package com.example.exchange.service;

import com.example.exchange.entity.Currency;
import com.example.exchange.exception.ExchangeException;
import com.example.exchange.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CurrencyServiceImpl implements CurrencyService{

    @Autowired
    private CurrencyRepository currencyRepository;

    public Currency getCurrency(String currencyCode) {
        return currencyRepository.findById(currencyCode)
                .orElseThrow(() -> new ExchangeException(String.format("No currency for currency code: %s", currencyCode)));
    }
}
