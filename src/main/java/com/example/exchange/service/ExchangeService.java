package com.example.exchange.service;

import com.example.exchange.entity.ExchangeTransaction;
import com.example.exchange.model.ExchangeRequest;

public interface ExchangeService {
    ExchangeTransaction exchange(ExchangeRequest exchangeRequest);

    ExchangeTransaction getExchangeTransaction(long exchangeTransactionId);
}
