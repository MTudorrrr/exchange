package com.example.exchange.service;

import com.example.exchange.entity.Employee;
import com.example.exchange.model.CreateEmployeeRequest;

public interface EmployeeService {
    Employee saveEmployee(CreateEmployeeRequest createEmployeeRequest);

    Employee getEmployee(Long employeeId);
}
