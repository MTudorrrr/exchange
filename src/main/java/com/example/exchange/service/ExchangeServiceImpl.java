package com.example.exchange.service;

import com.example.exchange.entity.*;
import com.example.exchange.exception.ExchangeException;
import com.example.exchange.model.ExchangeRequest;
import com.example.exchange.repository.*;
import com.example.exchange.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;

@Service
public class ExchangeServiceImpl implements  ExchangeService{

    @Autowired
    private RateServiceImpl rateService;

    @Autowired
    private BalanceTransactionRepository balanceTransactionRepository;

    @Autowired
    private ExchangeTransactionRepository exchangeTransactionRepository;

    @Autowired
    private BalanceServiceImpl balanceService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CurrencyServiceImpl currencyService;

    @Autowired
    private EmployeeServiceImpl employeeService;

    @Transactional
    public ExchangeTransaction exchange(ExchangeRequest exchangeRequest) {
        Rate rate = rateService.getRate(exchangeRequest.getSellCurrency(), exchangeRequest.getBuyCurrency());
        Employee employee = employeeService.getEmployee(exchangeRequest.getEmployeeId());
        double creditAmount = (exchangeRequest.getAmount() * rate.getRate());

        updateBalance(rate.getCurrencyPair(), exchangeRequest.getAmount(), creditAmount);

        Timestamp timestamp = new Timestamp(new Date().getTime());

        Transaction debitTransaction = Utils.createTransaction(rate.getCurrencyPair().getSellCurrency(), exchangeRequest.getAmount(), timestamp);
        Transaction creditTransaction = Utils.createTransaction(rate.getCurrencyPair().getBuyCurrency(), -creditAmount, timestamp);
        transactionRepository.save(debitTransaction);
        transactionRepository.save(creditTransaction);

        ExchangeTransaction exchangeTransaction = createExchangeTransaction(rate, employee, debitTransaction, creditTransaction);

        return exchangeTransactionRepository.save(exchangeTransaction);
    }

    private ExchangeTransaction createExchangeTransaction(Rate rate, Employee employee, Transaction debitTransaction, Transaction creditTransaction) {
        ExchangeTransaction exchangeTransaction = new ExchangeTransaction();
        exchangeTransaction.setCreditTransaction(creditTransaction);
        exchangeTransaction.setDebitTransaction(debitTransaction);
        exchangeTransaction.setEmployee(employee);
        exchangeTransaction.setRate(rate.getRate());
        return exchangeTransaction;
    }

    private void updateBalance(CurrencyPair currencyPair, double amountToDebit, double amountToCredit) {
        Balance debitBalance = balanceService.getBalance(currencyPair.getSellCurrency().getCode());
        Balance creditBalance = balanceService.getBalance(currencyPair.getBuyCurrency().getCode());

        if(creditBalance.getBalance() < amountToCredit) {
            throw new  ExchangeException(String.format("Not Enough %s balance",creditBalance.getCurrencyId()));
        }
        debitBalance.setBalance(debitBalance.getBalance() + amountToDebit);
        creditBalance.setBalance(creditBalance.getBalance() - amountToCredit);

        balanceService.saveBalance(debitBalance);
        balanceService.saveBalance(creditBalance);
    }

    public ExchangeTransaction getExchangeTransaction(long exchangeTransactionId) {
        return exchangeTransactionRepository.findById(exchangeTransactionId)
                .orElseThrow(() -> new ExchangeException(String.format("No exchange transaction for id: %d", exchangeTransactionId)));
    }
}
