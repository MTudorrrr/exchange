package com.example.exchange.service;

import com.example.exchange.entity.Currency;

public interface CurrencyService {
    Currency getCurrency(String currencyCode);
}
