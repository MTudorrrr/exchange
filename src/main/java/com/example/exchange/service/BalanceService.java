package com.example.exchange.service;

import com.example.exchange.entity.*;
import com.example.exchange.model.UpdateBalanceRequest;

public interface BalanceService {
    Balance getBalance(String currencyCode);

    Balance saveBalance(Balance balance);

    Balance updateBalance(UpdateBalanceRequest updateBalanceRequest);
}
