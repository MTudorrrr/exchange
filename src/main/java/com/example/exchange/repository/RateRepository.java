package com.example.exchange.repository;

import com.example.exchange.entity.CurrencyPair;
import com.example.exchange.entity.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateRepository extends JpaRepository<Rate, CurrencyPair> {

}
