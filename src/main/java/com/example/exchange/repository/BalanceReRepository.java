package com.example.exchange.repository;

import com.example.exchange.entity.Balance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BalanceReRepository extends JpaRepository<Balance, String> {

}
