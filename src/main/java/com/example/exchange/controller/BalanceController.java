package com.example.exchange.controller;

import com.example.exchange.entity.Balance;
import com.example.exchange.model.UpdateBalanceRequest;
import com.example.exchange.service.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "balance")
public class BalanceController {

    @Autowired
    BalanceService balanceService;

    @GetMapping(path = "/{currencyCode}")
    public Balance getBalance(@PathVariable String currencyCode) {
        return balanceService.getBalance(currencyCode);
    }

    @PutMapping
    public Balance updateBalance(@RequestBody UpdateBalanceRequest updateBalanceRequest) {
        return balanceService.updateBalance(updateBalanceRequest);
    }
}
