package com.example.exchange.controller;

import com.example.exchange.entity.ExchangeTransaction;
import com.example.exchange.model.ExchangeRequest;
import com.example.exchange.service.ExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "exchange")
public class ExchangeController {

    @Autowired
    ExchangeService exchangeService;

    @GetMapping(path = "transaction/{exchangeTransactionId}")
    public ExchangeTransaction getExchangeTransaction (@PathVariable long exchangeTransactionId){
        return exchangeService.getExchangeTransaction(exchangeTransactionId);
    }

    @PostMapping
    public ExchangeTransaction exchange(@RequestBody @Valid ExchangeRequest exchangeRequest){
       return exchangeService.exchange(exchangeRequest);
    }
}
