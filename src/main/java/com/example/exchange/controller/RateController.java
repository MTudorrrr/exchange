package com.example.exchange.controller;

import com.example.exchange.entity.Rate;
import com.example.exchange.model.UpdateRateRequest;
import com.example.exchange.service.RateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "rate")
public class RateController {

    @Autowired
    RateService rateService;

    @GetMapping
    public Rate getRate(@RequestParam String sellCurrency, @RequestParam String buyCurrency) {
        return rateService.getRate(sellCurrency, buyCurrency);
    }

    @GetMapping(path = "/all")
    public List<Rate> getAllRates() {
        return rateService.getAllRates();
    }

    @PostMapping
    public Rate createRate(@RequestBody UpdateRateRequest updateRateRequest) {
        return rateService.saveRate(updateRateRequest);
    }
}
