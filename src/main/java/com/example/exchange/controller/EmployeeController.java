package com.example.exchange.controller;

import com.example.exchange.entity.Employee;
import com.example.exchange.model.CreateEmployeeRequest;
import com.example.exchange.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @PostMapping
    public Employee createEmployee(@RequestBody CreateEmployeeRequest createEmployeeRequest) {
        return employeeService.saveEmployee(createEmployeeRequest);
    }

    @GetMapping(path = "/{employeeId}")
    public Employee getEmployee (@PathVariable long employeeId){
       return employeeService.getEmployee(employeeId);
    }
}
