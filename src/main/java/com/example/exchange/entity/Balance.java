package com.example.exchange.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "balance")
public class Balance {
    @Id
    @Column(name = "currencyid", nullable = false)
    private String currencyId;
    private double balance;
}
