package com.example.exchange.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Embeddable
public class CurrencyPair implements Serializable {
    @ManyToOne
    @JoinColumn(name = "sellcurrency", nullable = false)
    private Currency sellCurrency;

    @ManyToOne
    @JoinColumn(name = "buycurrency", nullable = false)
    private Currency buyCurrency;
}

