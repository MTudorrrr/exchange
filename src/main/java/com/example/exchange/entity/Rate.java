package com.example.exchange.entity;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "rate")
public class Rate {

    private double rate;
    @EmbeddedId
    private CurrencyPair currencyPair;
}
