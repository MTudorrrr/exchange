package com.example.exchange.entity;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "balancetransaction")
public class BalanceTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "operatorid", nullable = false)
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "transaction", nullable = false)
    private Transaction transaction;
}
