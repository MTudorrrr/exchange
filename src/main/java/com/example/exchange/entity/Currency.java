package com.example.exchange.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "currency")
public class Currency {
    @Id
    private String code;
    private String name;

    public Currency(String code) {
        this.code = code;
    }
}
