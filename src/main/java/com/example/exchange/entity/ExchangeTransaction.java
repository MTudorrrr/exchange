package com.example.exchange.entity;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "exchangetransaction")
public class ExchangeTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "operatorid", nullable = false)
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "credittransactionid", nullable = false)
    private Transaction creditTransaction;

    @ManyToOne
    @JoinColumn(name = "debittransactionid", nullable = false)
    private Transaction debitTransaction;

    private double rate;
}
