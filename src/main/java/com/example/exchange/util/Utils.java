package com.example.exchange.util;

import com.example.exchange.entity.Currency;
import com.example.exchange.entity.Transaction;

import java.sql.Timestamp;

public class Utils {


    public static Transaction createTransaction(Currency sellCurrency, double amount, Timestamp timestamp) {
        Transaction debitTransaction = new Transaction();
        debitTransaction.setAmount(amount);
        debitTransaction.setCurrency(sellCurrency);
        debitTransaction.setDatetime(timestamp);
        return debitTransaction;
    }
}
