package com.example.exchange.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;


@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Data
public class ExchangeException extends RuntimeException {
    String errorMessage;
}
