package com.example.exchange;

import com.example.exchange.exception.ExchangeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ControllerAdvisor {

    @ExceptionHandler(ExchangeException.class)
    public ResponseEntity handleExchangeException(ExchangeException exception) {
        ResponseEntity<String> response = new ResponseEntity<>(exception.getErrorMessage(), HttpStatus.NOT_FOUND);
        return response;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception exception) {
        log.warn(exception.getMessage());
        ResponseEntity<String> response = new ResponseEntity<>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
        return response;
    }
}
