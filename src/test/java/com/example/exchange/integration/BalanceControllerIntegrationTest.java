package com.example.exchange.integration;

import com.example.exchange.controller.BalanceController;
import com.example.exchange.entity.Balance;
import com.example.exchange.exception.ExchangeException;
import com.example.exchange.model.UpdateBalanceRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

@SpringBootTest
public class BalanceControllerIntegrationTest {
    //Some test data loaded from resources/data.sql

    @Autowired
    BalanceController balanceController;

    @Test
    void testGetAndUpdateBalance() {
        Balance balance = balanceController.getBalance("MDL");
        assert balance.getBalance() == 15372.5;
        assertEquals("MDL", balance.getCurrencyId());

        Balance updatedBalance = balanceController.updateBalance(new UpdateBalanceRequest(6, "MDL", 100));
        assert updatedBalance.getBalance() == 15472.5;
        assertEquals("MDL", updatedBalance.getCurrencyId());
    }

    @Test
    void testGetNonexistentBalance() {
        ExchangeException exchangeException = assertThrows(ExchangeException.class, () -> balanceController.getBalance("BTC"));
        assertEquals("No balance for currency code: BTC", exchangeException.getErrorMessage());
    }
}
