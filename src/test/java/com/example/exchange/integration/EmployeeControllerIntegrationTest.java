package com.example.exchange.integration;

import com.example.exchange.controller.EmployeeController;
import com.example.exchange.entity.Employee;
import com.example.exchange.model.CreateEmployeeRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class EmployeeControllerIntegrationTest {
    //Some test data loaded from resources/data.sql

    @Autowired
    EmployeeController employeeController;

    @Test
    void testAddAndGetEmployee() {
        CreateEmployeeRequest createEmployeeRequest = new CreateEmployeeRequest();
        createEmployeeRequest.setFirstName("testF");
        createEmployeeRequest.setLastName("testL");

        Employee savedEmployee = employeeController.createEmployee(createEmployeeRequest);

        assertEquals(9, savedEmployee.getId());
        assertEquals("testF", savedEmployee.getFirstName());
        assertEquals("testL", savedEmployee.getLastName());
    }

}
