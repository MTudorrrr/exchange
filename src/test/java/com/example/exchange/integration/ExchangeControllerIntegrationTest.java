package com.example.exchange.integration;

import com.example.exchange.controller.ExchangeController;
import com.example.exchange.entity.ExchangeTransaction;
import com.example.exchange.model.ExchangeRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
class ExchangeControllerIntegrationTest {
    //Some test data loaded from resources/data.sql

    @Autowired
    ExchangeController exchangeController;

    @Test
    void testGetExchangeTransaction() {
        ExchangeTransaction exchangeTransaction = exchangeController.getExchangeTransaction(1);
        assert 0.6 == exchangeTransaction.getRate();
        assertEquals(1, exchangeTransaction.getId());
        assertEquals(6, exchangeTransaction.getEmployee().getId());
        assertEquals(2, exchangeTransaction.getDebitTransaction().getId());
        assertEquals("2020-08-11 01:22:19.0", exchangeTransaction.getDebitTransaction().getDatetime().toString());
        assertEquals("MDL", exchangeTransaction.getDebitTransaction().getCurrency().getCode());
        assert 800 == exchangeTransaction.getDebitTransaction().getAmount();
        assertEquals(1, exchangeTransaction.getCreditTransaction().getId());
        assertEquals("2020-08-11 01:22:19.0", exchangeTransaction.getCreditTransaction().getDatetime().toString());
        assertEquals("USD", exchangeTransaction.getCreditTransaction().getCurrency().getCode());
        assert -480 == exchangeTransaction.getCreditTransaction().getAmount();
    }

    @Test
    void testExchange() {
        ExchangeRequest exchangeRequest = new ExchangeRequest(6, "USD", "MDL", 100);
        ExchangeTransaction exchangeTransaction = exchangeController.exchange(exchangeRequest);
        assert 16.0 == exchangeTransaction.getRate();
        assertEquals(6, exchangeTransaction.getEmployee().getId());
        assert -(100 * 16) == exchangeTransaction.getCreditTransaction().getAmount();
        assertEquals("MDL", exchangeTransaction.getCreditTransaction().getCurrency().getCode());
        assert 100 == exchangeTransaction.getDebitTransaction().getAmount();
        assertEquals("USD", exchangeTransaction.getDebitTransaction().getCurrency().getCode());
    }
}
