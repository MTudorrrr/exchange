package com.example.exchange.integration;

import com.example.exchange.controller.RateController;
import com.example.exchange.entity.Rate;
import com.example.exchange.model.UpdateRateRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.Assert.assertEquals;


@SpringBootTest
public class RateControllerIntegrationTest {
    //Some test data loaded from resources/data.sql

    @Autowired
    RateController rateController;

    @Test
    void testAddAndGetRate() {
        Rate rate = rateController.getRate("MDL", "USD");
        assert rate.getRate() == 0.06;
        assertEquals("MDL", rate.getCurrencyPair().getSellCurrency().getCode());
        assertEquals("USD", rate.getCurrencyPair().getBuyCurrency().getCode());

        rateController.createRate(new UpdateRateRequest("MDL", "USD", 1.5));
        Rate updatedRate = rateController.getRate("MDL", "USD");
        assert updatedRate.getRate() == 1.5;
        assertEquals("MDL", updatedRate.getCurrencyPair().getSellCurrency().getCode());
        assertEquals("USD", updatedRate.getCurrencyPair().getBuyCurrency().getCode());
    }

    @Test
    void testGetAllRates() {
        List<Rate> rates = rateController.getAllRates();
        assertEquals(2, rates.size());
    }
}
