package com.example.exchange.unit;

import com.example.exchange.entity.Currency;
import com.example.exchange.repository.CurrencyRepository;
import com.example.exchange.service.CurrencyService;
import com.example.exchange.service.CurrencyServiceImpl;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;


public class CurrencyServiceImplTest {

    CurrencyService currencyService;

    CurrencyRepository currencyRepository;

    @BeforeEach
    public void before() throws IllegalAccessException {
        currencyService = new CurrencyServiceImpl();
        currencyRepository = mock(CurrencyRepository.class);
        FieldUtils.writeField(currencyService, "currencyRepository", currencyRepository, true);
    }

    @Test
    public void test() {
        Currency currency = new Currency("MDL", "MDL");
        doReturn(Optional.of(currency)).when(currencyRepository).findById(anyString());
        Currency retrievedCurrency = currencyService.getCurrency("any");
        assertEquals(currency, retrievedCurrency);
    }
}
